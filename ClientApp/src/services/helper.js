﻿import { TOKEN, CURRENT_USER } from 'constants';

class Helper {
    static saveObject(name, obj) {
        const serializedObj = JSON.stringify(obj);
        localStorage.setItem(name, serializedObj);
    }

    static getObject(key) {
        const value = localStorage.getItem(key);
        return value && JSON.parse(value);
    }

    static authHeader() {
        // return authorization header with access token
        let token = this.getObject(TOKEN);

        if (token) {
            return {
                'Content-Type': 'application/json',
                'AccessToken': token
            };
        } else {
            return {};
        }
    }

    static authHeaderForFile() {
        // return authorization header with access token
        let token = this.getObject(TOKEN);

        if (token) {
            return {
                'AccessToken': token
            };
        } else {
            return {};
        }
    }

    static IsAuthenticated() {
        let loggedInuser = this.getObject(CURRENT_USER);
        if (loggedInuser) {
            return true;
        } else {
            return false;
        }
    }

    static GetCurrentUser() {
        let token = this.getObject(TOKEN);
        console.log(token);
        if (token) {
            return this.getObject(CURRENT_USER);
        } else {
            return null;
        }
    }

    static arrayToString(arr) {
        let str = '';
        arr.forEach(function (i, index) {
            str += i;
            if (index != (arr.length - 1)) {
                str += '';
            };
        });
        return str;
    }

    static Logout() {
        //localStorage.removeItem(TOKEN); 
        //localStorage.removeItem(CURRENT_USER); 
        localStorage.clear();
    }
}

export default Helper;