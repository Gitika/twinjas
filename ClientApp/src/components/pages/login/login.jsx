﻿import React, { Component } from 'react/index';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import { isEmpty } from 'lodash';
import { Button, InputGroup, Input, Form, Row } from 'reactstrap/dist/reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as authorizationThunk from '../../../redux/thunks/authorizationThunk';
import Helper from '../../../services/helper';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            errorMessages: '',
            loginInProgress: false,
            invalidEmail: false,
            invalidPassword: false
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleInputChange(event) {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
            errorMessages: '',
            invalidEmail: false,
            invalidPassword: false,
            loginInProgress: false
        });
    }

    handleLogin(event) {
        event.preventDefault();
        const { email, password } = this.state;
        const { action, history } = this.props;

        let isValid = true;

        const messages = [];

        if (isEmpty(email)) {
            isValid = false;
            messages.push("- Please enter username\n");
        }

        if (isEmpty(password)) {
            isValid = false;
            messages.push("- Please enter password\n");
        }

        if (!isValid) {
            this.setState({ loading: false });
            alert(Helper.arrayToString(messages));
            return;
        }

        this.setState({
            loginInProgress: true
        });

        history.push('/dashboard');

        //action.login(email, password).then(
        //    res => {
        //        if (res != undefined && (res.isAuthenticated && res.isValid)) {
        //            history.push('/dashboard');
        //        }
        //        else
        //            alert(res.message);
        //    }
        //);
    }

    render() {
        const { email, password, invalidEmail, loginInProgress, invalidPassword, errorMessages } = this.state;

        return (
            <div>
                <Helmet>
                    <title>Twinjas :: Login</title>
                </Helmet>
                <p>{errorMessages}</p>
                <Form onSubmit={this.handleLogin}>
                    <div class="form-group">
                        <div class="position-relative has-icon-right">
                            <label for="email" class="sr-only">Username</label>
                            <input type="text" id="email" name="email" value={email} onChange={this.handleInputChange} class="form-control form-control-rounded" placeholder="Username" />
                            <div class="form-control-position">
                                <i class="icon-user"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="position-relative has-icon-right">
                            <label for="password" class="sr-only">Password</label>
                            <input type="password" id="password" name="password" value={password} onChange={this.handleInputChange} class="form-control form-control-rounded" placeholder="Password" />
                            <div class="form-control-position">
                                <i class="icon-lock"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mr-0 ml-0">
                        <div class="form-group col-6">
                            &nbsp;
                        </div>
                        <div class="form-group col-6 text-right">
                            <a href="authentication-reset-password.html">Reset Password</a>
                        </div>
                    </div>
                    <Button disable={loginInProgress} color="tt-blue" type="submit" className="btn btn-primary shadow-primary btn-round btn-block waves-effect waves-light">
                        Sign In
                    </Button>
                    <div class="text-center pt-3" style={{ display: "none" }}>
                        <p>or Sign in with</p>
                        <a href="javascript:void()" class="btn-social btn-social-circle btn-facebook waves-effect waves-light m-1"><i class="fa fa-facebook"></i></a>
                        <a href="javascript:void()" class="btn-social btn-social-circle btn-google-plus waves-effect waves-light m-1"><i class="fa fa-google-plus"></i></a>
                        <a href="javascript:void()" class="btn-social btn-social-circle btn-twitter waves-effect waves-light m-1"><i class="fa fa-twitter"></i></a>
                        <hr />
                        <p class="text-muted">Do not have an account? <a href="authentication-signup.html"> Sign Up here</a></p>
                    </div>
                </Form>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        Message: (state.authorization) ? state.authorization.message : "",
        history: ownProps.history
    };
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(authorizationThunk, dispatch),
        dispatch
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);