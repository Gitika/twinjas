﻿import * as React from 'react';
import LeftMenu from "./leftMenu";
import PageHeader from "./pageHeader";
import '../../styles/animate.css';
import '../../styles/app-style.css';
import '../../styles/bootstrap.min.css';
import '../../styles/icons.css';
import '../../styles/sidebar-menu.css';
import 'hammerjs';
import '../../scripts/jquery.min.js';
import '../../scripts/popper.min.js';
import '../../scripts/bootstrap.min.js';
import '../../plugins/simplebar/js/simplebar.js';
import '../../scripts/waves.js';
import '../../scripts/sidebar-menu.js';
import '../../scripts/app-script.js';

import Helper from '../../services/helper';

class mainLayout extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mobileView: false,
            shoeLeftMenu: true
        };

        this.logout = this.logout.bind(this);
        this.updateViewState = this.updateViewState.bind(this);
        this.toggleSideBar = this.toggleSideBar.bind(this);
    }

    logout() {
        const { history } = this.props.children.props;
        Helper.Logout();
        history.push('/');
        //this.props.history.push('/login');
    }

    componentDidMount() {
        this.updateViewState();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.children.props.location.key !== this.props.children.props.location.key) {
            this.updateViewState();
        }
    }

    updateViewState() {
        if (window.navigator.userAgent.match(/Android/i)
            || window.navigator.userAgent.match(/webOS/i)
            || window.navigator.userAgent.match(/iPhone/i)
            || window.navigator.userAgent.match(/iPad/i)
            || window.navigator.userAgent.match(/iPod/i)
            || window.navigator.userAgent.match(/BlackBerry/i)
            || window.navigator.userAgent.match(/Windows Phone/i)
        ) {
            console.log("Tablet: " + "mobile() ");
            this.setState({
                mobileView: true,
                shoeLeftMenu: false
            });
        } else if (document.documentElement.clientWidth === 1536 && document.documentElement.clientHeight === 2048) {
            this.setState({
                mobileView: true,
                shoeLeftMenu: false
            });
        } else {
            this.setState({
                mobileView: false,
                shoeLeftMenu: true
            });
        }
    }

    toggleSideBar() {
        this.setState({
            shoeLeftMenu: !this.state.shoeLeftMenu
        });
    }

    render() {
        const { children } = this.props;
        let containerClass = '';

        if (!this.state.shoeLeftMenu) containerClass = 'enlarged';

        return (
            <div className={containerClass}>
                <div id="wrapper">
                    <>
                        <LeftMenu />
                        <PageHeader onClickMenuToggel={this.toggleSideBar} logout={this.logout} />
                        <div class="clearfix"></div>
                        <div className="content-wrapper">
                            <div className="container-fluid" style={{ height: "610px" }}>
                                {children}
                            </div>
                        </div>
                        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
                        <footer class="footer">
                            <div class="container">
                                <div class="text-center">
                                    Copyright © 2019 Twinjas
                                </div>
                            </div>
                        </footer>
                    </>
                </div>
            </div>
        );
    }
}

export default mainLayout;