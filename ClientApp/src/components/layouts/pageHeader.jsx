﻿import * as React from 'react';
import { Link } from 'react-router-dom';
import '../../styles/animate.css';
import '../../styles/app-style.css';
import '../../styles/bootstrap.min.css';
import '../../styles/icons.css';
import '../../styles/sidebar-menu.css';
import userImg from '../../images/avatar-1.jpg';
import Helper from '../../services/helper';

class pageHeader extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            popoverOpen: false
        };

        this.toggle = this.toggle.bind(this);
        this.ShowHideMenu = this.ShowHideMenu.bind(this);
    }

    toggle() {
        const { popoverOpen } = this.state;
        this.setState({
            popoverOpen: !popoverOpen
        });
    }

    ShowHideMenu(e) {
        this.props.onClickMenuToggel();
    }

    render() {
        return (
            <header class="topbar-nav">
                <nav class="navbar navbar-expand fixed-top bg-white">
                    <ul class="navbar-nav mr-auto align-items-center">
                        <li class="nav-item">
                            <a class="nav-link toggle-menu" href="javascript:void();">
                                <i class="icon-menu menu-icon"></i>
                            </a>
                        </li>
                        <li class="nav-item" style={{ display: 'none' }}>
                            <form class="search-bar">
                                <input type="text" class="form-control" placeholder="Enter keywords" />
                                <a href="javascript:void();"><i class="icon-magnifier"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav align-items-center right-nav-link">
                        <li class="nav-item language">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="#"><i class="flag-icon flag-icon-gb"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item"> <i class="flag-icon flag-icon-gb mr-2"></i> English</li>
                                <li class="dropdown-item"> <i class="flag-icon flag-icon-de mr-2"></i> German</li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="javascript:void(0)">
                                <span class="user-profile">
                                    <img src={userImg} alt="user" className="img-circle" />
                                </span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item user-details">
                                    <a href="javaScript:void();">
                                        <div class="media">
                                            <div class="avatar"><img class="align-self-start mr-3" src={userImg} alt="user avatar" /></div>
                                            <div class="media-body">
                                                <h6 class="mt-2 user-title">Jenny Methew</h6>
                                                <p class="user-subtitle">jenny@gmail.com</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="dropdown-divider"></li>
                                <li class="dropdown-item"><i class="icon-wallet mr-2"></i> Profile</li>
                                <li class="dropdown-divider"></li>
                                <li class="dropdown-item"><i class="icon-power mr-2"></i>
                                    <Link to="#" onClick={this.props.logout}>Logout</Link>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default pageHeader;