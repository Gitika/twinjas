﻿import * as React from 'react';
import { Redirect, Route } from 'react-router-dom';
import Helper from '../../services/helper';

const layoutRoute = ({ component: Component, layout: Layout, ...rest }) => {
    return (
        <Route {...rest}
            render={props => (
                //Helper.IsAuthenticated() && Helper.GetCurrentUser()
                //    ? (
                        <Layout>
                            <Component {...props} />
                        </Layout >
                    //)
                   // : <Redirect to="/login" />
            )}
        />
    );
};

export default layoutRoute;