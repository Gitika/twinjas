﻿import * as React from 'react';
import { Nav, NavItem, NavLink as BSNavLink, } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import '../../styles/animate.css';
import '../../styles/app-style.css';
import '../../styles/bootstrap.min.css';
import '../../styles/icons.css';
import '../../styles/sidebar-menu.css';
import logo from '../../images/logo-icon.png';
import helper from '../../services/helper';

const leftMenu = (e) => {
    const sidebarItem = [
        {
            name: 'Dashboard',
            href: '/dashboard',
            class: 'icon-home'
        },
        {
            name: 'App Users',
            href: '/appUsers',
            class: 'icon-user'
        }
    ];

    return (
        <aside>
            <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
                <div class="brand-logo">
                    <img src={logo} class="logo-icon" alt="logo icon" />
                    <h5 class="logo-text">Twinjas</h5>
                </div>
                <p>&nbsp;</p>
                <Nav class="sidebar-menu do-nicescrol">
                    {
                        (sidebarItem.map((item, index) => {
                            const className = item.class;
                            return (
                                <NavItem key={index}>
                                    <BSNavLink to={item.href} tag={NavLink} className="waves-effect" ><i className={className}></i>&nbsp;<span>{item.name}</span></BSNavLink>
                                </NavItem>
                            );
                        }))
                    }
                </Nav>
            </div>
            <div class="clearfix"></div>
        </aside>
    );
}

export default leftMenu;




