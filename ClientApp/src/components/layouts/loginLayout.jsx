﻿import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';
import '../../styles/animate.css';
import '../../styles/app-style.css';
import '../../styles/bootstrap.min.css';
import '../../styles/icons.css';
import '../../scripts/jquery.min.js';
import '../../scripts/popper.min.js';
import '../../scripts/bootstrap.min.js';
import logo from '../../images/logo-icon.png';

import Helper from '../../services/helper';

const loginLayout = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props => (
                Helper.IsAuthenticated() && Helper.GetCurrentUser()
                    ? <Redirect to="/dashboard" />
                    : <div id="wrapper">
                        <div class="card border-primary border-top-sm border-bottom-sm card-authentication1 mx-auto my-5 animated bounceInDown">
                            <div class="card-body">
                                <div class="card-content p-2">
                                    <div class="text-center">
                                        <img src={logo} />
                                    </div>
                                    <div class="card-title text-uppercase text-center py-3">Sign In</div>
                                    <Component {...props} />
                                </div>
                            </div>
                        </div>
                    </div>
            )}
        />
    );
};
export default loginLayout;