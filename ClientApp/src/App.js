import React, { Component } from 'react';
import { Switch } from 'react-router-dom';

import LayoutRoute from './components/layouts/layoutRoute';
import LoginLayout from './components/layouts/loginLayout';
import MainLayout from './components/layouts/mainLayout';

import Login from './components/pages/login/login';
import Dashboard from './components/pages/dashboard/dashboard';
import AppUserList from './components/pages/appUsers/appUserList';

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Switch>
                <LoginLayout
                    exact={true}
                    path="/"
                    component={Login}
                />
                <LayoutRoute
                    exact={true}
                    path="/dashboard"
                    layout={MainLayout}
                    component={Dashboard}
                />
                <LayoutRoute
                    exact={true}
                    path="/appUsers"
                    layout={MainLayout}
                    component={AppUserList}
                />
            </Switch>
        );
    }
}
