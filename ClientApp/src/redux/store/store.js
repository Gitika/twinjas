﻿import { applyMiddleware, compose, createStore } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';


import rootReducer from '../reducers/rootReducer';
import helper from '../../services/helper';
import { TOKEN } from '../../services/constants';




const init = {
    token: ''
};

function checkLocalStorage() {
    const initialState = { ...init };
    initialState.token = helper.getObject(TOKEN) || init.TOKEN;
    return initialState;
}

export default function store(initialState) {
    const initState = initialState || checkLocalStorage();
    return createStore(rootReducer, initialState, compose(
        // Add other middleware on this line...
        applyMiddleware(reduxImmutableStateInvariant()),
        window.devToolsExtension ? window.devToolsExtension() : f => f // add support for Redux dev tools
    ));
}